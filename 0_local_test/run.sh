# makeHistograms.py \
# --input /home/newhouse/public/Analysis/HNL/DHNLAlgorithm/run/13_systematics/group.phys-exotics.mc16_13TeV.311633.DAOD_EXOT29.e7422_e5984_a875_r11915_r11748_p4482.dhnl_ntuple_v7p0.v1_tree.root/data-tree/files_a.root \
# --config /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/data/mc/config_mc_uuu.json \
# --output_file /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/run/0_local_test/output_uuu.root \
# --analysis run2Analysis \
# --nevents 100 \
# --saveNtuples mHNL \
# --force 

makeHistograms.py \
--input /data/newhouse/forNicky/submit_dir_data15/data-tree/files15.root \
--config /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/data/mc/config_mc_uuu.json \
--output_file /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/run/0_local_test/output_debug_data15_uuu.root \
--analysis run2Analysis \
--saveNtuples mHNL \
--force 

