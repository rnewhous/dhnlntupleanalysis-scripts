python ~/public/Analysis/HNL/DHNLNtupleAnalysis/python/makeHistograms.py --config ~/public/Analysis/HNL/DHNLNtupleAnalysis/data/config_mc_kshort.json \
--input /data/hnl/KShort/dijet_susy15_ntuples/user.rnewhous.user.rnewhous.364712.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12WithSW.mc16e.ntuple.v2_tree.root/ntuples_merged.root \
--force \
--analysis KShort \
--saveNtuple sel \
--output .

# --nevents 1000 \
