import os
import glob
import time

##### MC SETUP #####
# ntuple_files = glob.glob("/data/hnl/KShort/ntuples/dijet/JZ9/*/submitDir/data-tree/files.root")
ntuple_files = glob.glob("/data/hnl/KShort/ntuples/dijet/JZ8/*.root")
for ntuple_file in ntuple_files:
    subdir = '/'.join(ntuple_file.split('/')[:-3]).replace('ntuples', 'histograms')+'/'
    jz = ntuple_file.split('/')[6]
    os.makedirs(subdir, exist_ok=True)
histogram_path = f'/data/hnl/KShort/histograms/dijet/{jz}/'
data_or_mc = 'mc'

# ##### DATA SETUP #####
# ntuple_files = glob.glob("/data/hnl/data18_ntuples/user.rnewhous.data18_13TeV.periodAllYear.physics_Main.PhysCont.dHNLNtups.pro25_v04_combLRTd0Test_1p0ONLY_v1_tree.root/*.tree.root")
# histogram_path = '/data/hnl/KShort/histograms/data/'
# ntuple_files = sorted(ntuple_files, key=os.path.getsize)
# data_or_mc = 'data'

for i, ntuple_file in enumerate(ntuple_files):
    subdir = f'{histogram_path}{i}/'
    os.makedirs(subdir, exist_ok=True)


    command = f"""
#PBS -N dijet_hist_{i}_{jz}
#PBS -l walltime=48:00:00
#PBS -l nodes=1:ppn=1
#PBS -e /home/newhouse/tmp/grid_logs/dijet_hist_{i}_{jz}.err
#PBS -o /home/newhouse/tmp/grid_logs/dijet_hist_{i}_{jz}.log

source /home/newhouse/setup/pbs_setup.sh
lsetup git

cd {subdir}
source /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/setup.sh

python /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/python/makeHistograms.py \\
--input {ntuple_file} \\
--config /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/data/config_{data_or_mc}_kshort.json \\
--output {subdir}output \\
--analysis KShort \\
--force \\
--notHNLmc \\
--saveNtuples mass
"""

    with open(subdir+'submit.pbs', 'w') as f:  
        print(command, file=f)

    print(f"Submitting {subdir}submit.pbs")
    # print(command)
    # exit(0)
    os.system(f'qsub {subdir}submit.pbs')
    print("Finished")
    time.sleep(0.33)

    max_job_count=250
    def run_count(): return int(os.popen('qstat -r|  wc -l').read().strip())
    def idle_count(): return int(os.popen('qstat -i|  wc -l').read().strip())
    while run_count() + idle_count() > max_job_count:
        time.sleep(10)
