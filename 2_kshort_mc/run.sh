python /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/python/makeHistograms.py \
--input files.root \
--config config_mc_kshort.json \
--output output \
--analysis KShort \
--force \
--nevents 100 \
--notHNLmc