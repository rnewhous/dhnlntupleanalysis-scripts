function getRunCount {
        qstat -r|  wc -l
}
function getIdleCount {
        qstat -i|  wc -l
}
### Alignment Off
# SAMPLE_DIR=/data/hnl/data_rep_ntuples/alignment_off/user.rnewhous.data15_13TeV.00284427.physics_Main.merge.DAOD_RPVLL.r11761_r11764_p4054_HNLNtuple_01_tree.root/
# SAMPLE_BASE=user.rnewhous.21711119.merged.tree.root

# SAMPLE_DIR=/data/hnl/data_rep_ntuples/alignment_off/user.rnewhous.data16_13TeV.00304178.physics_Main.merge.DAOD_RPVLL.r11761_r11764_p4054_HNLNtuple_02_tree.root/
# SAMPLE_BASE=user.rnewhous.21711102.merged.tree.root

SAMPLE_DIR=/data/hnl/data_rep_ntuples/alignment_off/user.rnewhous.data17_13TeV.00340368.physics_Main.merge.DAOD_RPVLL.r11761_r11764_p4054_HNLNtuple_01_tree.root/
SAMPLE_BASE=user.rnewhous.21710573.merged.tree.root

# SAMPLE_DIR=/data/hnl/data_rep_ntuples/alignment_off/user.rnewhous.data18_13TeV.00358031.physics_Main.merge.DAOD_RPVLL.r11760_r11764_p4054_HNLNtuple_01_tree.root/
# SAMPLE_BASE=user.rnewhous.21711047.merged.tree.root

### Alignment On
# SAMPLE_DIR=/data/hnl/data_rep_ntuples/alignment_on/user.rnewhous.data15_13TeV.00284427.physics_Main.merge.DAOD_RPVLL.r11969_r11784_p4072_HNLNtuple_01_tree.root/
# SAMPLE_BASE=user.rnewhous.21753326.merged.tree.root

# SAMPLE_DIR=/data/hnl/data_rep_ntuples/alignment_on/user.rnewhous.data16_13TeV.00304178.physics_Main.recon.DAOD_RPVLL.r11969_r11784_p4072_HNLNtuple_01_tree.root/
# SAMPLE_BASE=user.rnewhous.21800937.merged.tree.root

# SAMPLE_DIR=
# SAMPLE_BASE=

# SAMPLE_DIR=/data/hnl/data_rep_ntuples/alignment_on/user.rnewhous.data18_13TeV.00358031.physics_Main.merge.DAOD_RPVLL.r11969_r11784_p4072_HNLNtuple_01_tree.root/
# SAMPLE_BASE=user.rnewhous.21753977.merged.tree.root


N_SPLIT_FILES=$(ls -1 ${SAMPLE_DIR}${SAMPLE_BASE}_split* | wc -l)
echo $N_SPLIT_FILES

# prepare jobs
# N_SPLIT_FILES=3
submissionDir=${SAMPLE_BASE}_sub/
rm -r $submissionDir  
mkdir -p $submissionDir
for (( i=0; i<$N_SPLIT_FILES; i++ ))
do
    inFile=${SAMPLE_DIR}${SAMPLE_BASE}_split$i
    mkdir -p ${SAMPLE_DIR}histograms/
    outDir=${SAMPLE_DIR}histograms/${SAMPLE_BASE}_split$i/
    # ls ${SAMPLE_DIR}${SAMPLE_BASE}_split$i
    tempPbsFile=$submissionDir${SAMPLE_BASE}_split$i.pbs
    echo "#PBS -N Job_name" > $tempPbsFile
    echo "#PBS -l walltime=20:00:00" >> $tempPbsFile
    echo "#PBS -l nodes=1:ppn=1" >> $tempPbsFile
    mkdir -p /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/run/batch_logs/
    echo "#PBS -o /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/run/batch_logs/" >> $tempPbsFile
    echo "#PBS -e /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/run/batch_logs/" >> $tempPbsFile
    echo "#PBS -N ${SAMPLE_BASE}_split$i" >> $tempPbsFile

    # setup environment
    echo ". /home/newhouse/miniconda3/etc/profile.d/conda.sh" >> $tempPbsFile
    echo "conda activate" >> $tempPbsFile

    # run command
    echo "python /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/python/makeHistograms.py \
    --config /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/data/config_data_alignment.json \
    --input  $inFile \
    --force --analysis FilterCompareData --output $outDir" >> $tempPbsFile
done

# submit jobs
maxJobCount=288
for pbsFile in $(ls $submissionDir)
do
    qsub $submissionDir$pbsFile
    runCount="$(getRunCount)"
    idleCount="$(getIdleCount)"
    jobCount=$(( $runCount + $idleCount ))
    echo "job count"
    echo $jobCount

    while [ $jobCount -gt $maxJobCount ]; do ## only
        sleep 30
        runCount="$(getRunCount)"
        idleCount="$(getIdleCount)"
        jobCount=$(( $runCount + $idleCount ))
    done
done
