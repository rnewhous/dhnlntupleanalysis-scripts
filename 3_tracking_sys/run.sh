python /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/python/makeHistograms.py \
--input /data/hnl/ntuples/tracking_syst_ntuples_test/eee/data-tree/eee_files.root \
--config /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/data/mc/config_mc_eee.json \
--output_file /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/run/3_tracking_sys/output_eee.root \
--analysis run2Analysis \
--force &

python /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/python/makeHistograms.py \
--input /data/hnl/ntuples/tracking_syst_ntuples_test/eee_dropped/data-tree/eee_files.root \
--config /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/data/mc/config_mc_eee.json \
--output_file /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/run/3_tracking_sys/output_eee_dropped.root \
--analysis run2Analysis \
--force &

python /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/python/makeHistograms.py \
--input /data/hnl/ntuples/tracking_syst_ntuples_test/eeu/data-tree/eeu_files.root \
--config /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/data/mc/config_mc_eeu.json \
--output_file /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/run/3_tracking_sys/output_eeu.root \
--analysis run2Analysis \
--force &

python /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/python/makeHistograms.py \
--input /data/hnl/ntuples/tracking_syst_ntuples_test/eeu_dropped/data-tree/eeu_files.root \
--config /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/data/mc/config_mc_eeu.json \
--output_file /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/run/3_tracking_sys/output_eeu_dropped.root \
--analysis run2Analysis \
--force &

python /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/python/makeHistograms.py \
--input /data/hnl/ntuples/tracking_syst_ntuples_test/uue/data-tree/uue_files.root \
--config /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/data/mc/config_mc_uue.json \
--output_file /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/run/3_tracking_sys/output_uue.root \
--analysis run2Analysis \
--force &

python /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/python/makeHistograms.py \
--input /data/hnl/ntuples/tracking_syst_ntuples_test/uue_dropped/data-tree/uue_files.root \
--config /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/data/mc/config_mc_uue.json \
--output_file /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/run/3_tracking_sys/output_uue_dropped.root \
--analysis run2Analysis \
--force &

python /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/python/makeHistograms.py \
--input /data/hnl/ntuples/tracking_syst_ntuples_test/uuu/data-tree/uuu_files.root \
--config /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/data/mc/config_mc_uuu.json \
--output_file /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/run/3_tracking_sys/output_uuu.root \
--analysis run2Analysis \
--force &

python /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/python/makeHistograms.py \
--input /data/hnl/ntuples/tracking_syst_ntuples_test/uuu_dropped/data-tree/uuu_files.root \
--config /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/data/mc/config_mc_uuu.json \
--output_file /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/run/3_tracking_sys/output_uuu_dropped.root \
--analysis run2Analysis \
--force &
