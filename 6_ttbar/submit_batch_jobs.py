import os
import time

# channel = 'eeu'

for channel in ['uuu', 'uue', 'eeu', 'eee']:
    # make input files using: for i in $(ls -1d /data/hnl/ntuples/v5p0_ntuples/mc/*); do ls $i/* -1d; done > input_files.txt
    # os.system("for i in $(ls -1d /data/hnl/ntuples/v4p1_ntuples/mc/*); do ls $i/* -1d; done > input_files.txt")
    os.system("for i in $(ls -1d /data/hnl/ntuples/ttbar_v5p0_ntuples/*/data-tree/); do ls $i/* -1d; done > input_files.txt")
    with open('input_files.txt') as f:
        for infile in f.readlines():
            infile = infile.strip()
            # get mc generator info
            dsid = int(infile.split('.')[3])
            # get reconstruction info
            rtag = infile.split('_')[6]
            # make output string
            # outstring = f'{rtag_info[rtag]}_{mc_info[dsid][0]}_{mc_info[dsid][1]}_{mc_info[dsid][2].replace("lt","").replace("dd","mm")}'
            outstring = infile[37:43]
            outdir = '/data/hnl/histograms/ttbar_v5p1_histograms/'
            outfile = outdir+'histograms_'+outstring+f'_{channel}.root'
            # skip if already done
            if os.path.isfile(outfile): continue
            # get the correct config file
            # config = config_info[mc_info[dsid][0]]
            config = f'/home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/run/6_ttbar/ttbar_config_{channel}.json'
            # set command with the correct input variables
            command  = f'qsub -v INPUT_FILE={infile},CONFIG_FILE={config},OUTPUT_FILE={outfile} -N ntuple_analysis_{outstring} -e /home/newhouse/tmp/grid_logs/ntuple_analysis_{outstring}.err -o /home/newhouse/tmp/grid_logs/ntuple_analysis_{outstring}.log /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/run/6_ttbar/run_grid.pbs'
            # run
            # print(command)
            exit_code = os.system(command)
            # success?
            print(exit_code, 'running', outstring)

            # time.sleep(0.01)

            # max_job_count=300
            # def run_count(): return int(os.popen('qstat -r|  wc -l').read().strip())
            # def idle_count(): return int(os.popen('qstat -i|  wc -l').read().strip())
            # while run_count() + idle_count() > max_job_count:
            #     time.sleep(10)
