makeHistograms.py \
--input /data/hnl/ntuples/ttbar_v5p0_ntuples/user.rnewhous.mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.EXOT29.r11916.v1_EXT0.root \
--config ttbar_config.json \
--output output \
--notHNLmc \
--nevents 100 \
--force 