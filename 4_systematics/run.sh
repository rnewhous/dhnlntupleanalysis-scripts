makeHistograms.py \
--input /data/hnl/ntuples/v5p0_ntuples/mc/group.phys-exotics.mc16_13TeV.313015.DAOD_EXOT29.e7902_e5984_a875_r11891_r11748_p4482.dhnl_ntuple_v5p0.v2_tree.root/group.phys-exotics.25399627._000001.tree.root \
--config $WorkDir/data/mc/config_mc_uuu.json \
--output output \
--force 