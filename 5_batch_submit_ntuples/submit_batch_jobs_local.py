import os
import time
import subprocess
import glob

mc_info = {}
mc_info[311602] = [ "uuu", "3G", "lt1dd"]
mc_info[311603] = [ "uuu", "3G", "lt10dd"]
mc_info[311604] = [ "uuu", "3G", "lt100dd"]
mc_info[311605] = [ "uue", "3G", "lt1dd"]
mc_info[311606] = [ "uue", "3G", "lt10dd"]
mc_info[311607] = [ "uue", "3G", "lt100dd"]
mc_info[311608] = [ "uuu", "4G", "lt1dd"]
mc_info[311609] = [ "uuu", "4G", "lt10dd"]
mc_info[311610] = [ "uuu", "4G", "lt100dd"]
mc_info[311611] = [ "uue", "4G", "lt1dd"]
mc_info[311612] = [ "uue", "4G", "lt10dd"]
mc_info[311613] = [ "uue", "4G", "lt100dd"]
mc_info[311614] = [ "uuu", "4p5G", "lt1dd"]
mc_info[311615] = [ "uuu", "4p5G", "lt10dd"]
mc_info[311616] = [ "uuu", "4p5G", "lt100dd"]
mc_info[311617] = [ "uue", "4p5G", "lt1dd"]
mc_info[311618] = [ "uue", "4p5G", "lt10dd"]
mc_info[311619] = [ "uue", "4p5G", "lt100dd"]
mc_info[311620] = [ "uuu", "5G", "lt1dd"]
mc_info[311621] = [ "uuu", "5G", "lt10dd"]
mc_info[311622] = [ "uuu", "5G", "lt100dd"]
mc_info[311623] = [ "uue", "5G", "lt1dd"]
mc_info[311624] = [ "uue", "5G", "lt10dd"]
mc_info[311625] = [ "uue", "5G", "lt100dd"]
mc_info[311626] = [ "uuu", "7p5G", "lt1dd"]
mc_info[311627] = [ "uuu", "7p5G", "lt10dd"]
mc_info[311628] = [ "uuu", "7p5G", "lt100dd"]
mc_info[311629] = [ "uue", "7p5G", "lt1dd"]
mc_info[311630] = [ "uue", "7p5G", "lt10dd"]
mc_info[311631] = [ "uue", "7p5G", "lt100dd"]
mc_info[311632] = [ "uuu", "10G", "lt1dd"]
mc_info[311633] = [ "uuu", "10G", "lt10dd"]
mc_info[311634] = [ "uuu", "10G", "lt100dd"]
mc_info[311635] = [ "uue", "10G", "lt1dd"]
mc_info[311636] = [ "uue", "10G", "lt10dd"]
mc_info[311637] = [ "uue", "10G", "lt100dd"]
mc_info[311638] = [ "uuu", "12p5G", "lt1dd"]
mc_info[311639] = [ "uuu", "12p5G", "lt10dd"]
mc_info[311640] = [ "uuu", "12p5G", "lt100dd"]
mc_info[311641] = [ "uue", "12p5G", "lt1dd"]
mc_info[311642] = [ "uue", "12p5G", "lt10dd"]
mc_info[311643] = [ "uue", "12p5G", "lt100dd"]
mc_info[311644] = [ "uuu", "15G", "lt1dd"]
mc_info[311645] = [ "uuu", "15G", "lt10dd"]
mc_info[311646] = [ "uuu", "15G", "lt100dd"]
mc_info[311647] = [ "uue", "15G", "lt1dd"]
mc_info[311648] = [ "uue", "15G", "lt10dd"]
mc_info[311649] = [ "uue", "15G", "lt100dd"]
mc_info[311650] = [ "uuu", "17p5G", "lt1dd"]
mc_info[311651] = [ "uuu", "17p5G", "lt10dd"]
mc_info[311652] = [ "uuu", "17p5G", "lt100dd"]
mc_info[311653] = [ "uue", "17p5G", "lt1dd"]
mc_info[311654] = [ "uue", "17p5G", "lt10dd"]
mc_info[311655] = [ "uue", "17p5G", "lt100dd"]
mc_info[311656] = [ "uuu", "20G", "lt1dd"]
mc_info[311657] = [ "uuu", "20G", "lt10dd"]
mc_info[311658] = [ "uuu", "20G", "lt100dd"]
mc_info[311659] = [ "uue", "20G", "lt1dd"]
mc_info[311660] = [ "uue", "20G", "lt10dd"]
mc_info[311661] = [ "uue", "20G", "lt100dd"]
mc_info[312956] = ["eee", "3G", "lt1dd"]
mc_info[312957] = ["eee", "3G", "lt10dd"]
mc_info[312958] = ["eee", "3G", "lt100dd"]
mc_info[312959] = ["eeu", "3G", "lt1dd"]
mc_info[312960] = ["eeu", "3G", "lt10dd"]
mc_info[312961] = ["eeu", "3G", "lt100dd"]
mc_info[312962] = ["eee", "4G", "lt1dd"]
mc_info[312963] = ["eee", "4G", "lt10dd"]
mc_info[312964] = ["eee", "4G", "lt100dd"]
mc_info[312965] = ["eeu", "4G", "lt1dd"]
mc_info[312966] = ["eeu", "4G", "lt10dd"]
mc_info[312967] = ["eeu", "4G", "lt100dd"]
mc_info[312968] = ["eee", "4p5G", "lt1dd"]
mc_info[312969] = ["eee", "4p5G", "lt10dd"]
mc_info[312970] = ["eee", "4p5G", "lt100dd"]
mc_info[312971] = ["eeu", "4p5G", "lt1dd"]
mc_info[312972] = ["eeu", "4p5G", "lt10dd"]
mc_info[312973] = ["eeu", "4p5G", "lt100dd"]
mc_info[312974] = ["eee", "5G", "lt1dd"]
mc_info[312975] = ["eee", "5G", "lt10dd"]
mc_info[312976] = ["eee", "5G", "lt100dd"]
mc_info[312977] = ["eeu", "5G", "lt1dd"]
mc_info[312978] = ["eeu", "5G", "lt10dd"]
mc_info[312979] = ["eeu", "5G", "lt100dd"]
mc_info[312980] = ["eee", "7p5G", "lt1dd"]
mc_info[312981] = ["eee", "7p5G", "lt10dd"]
mc_info[312982] = ["eee", "7p5G", "lt100dd"]
mc_info[312983] = ["eeu", "7p5G", "lt1dd"]
mc_info[312984] = ["eeu", "7p5G", "lt10dd"]
mc_info[312985] = ["eeu", "7p5G", "lt100dd"]
mc_info[312986] = ["eee", "10G", "lt1dd"]
mc_info[312987] = ["eee", "10G", "lt10dd"]
mc_info[312988] = ["eee", "10G", "lt100dd"]
mc_info[312989] = ["eeu", "10G", "lt1dd"]
mc_info[312990] = ["eeu", "10G", "lt10dd"]
mc_info[312991] = ["eeu", "10G", "lt100dd"]
mc_info[312992] = ["eee", "12p5G", "lt1dd"]
mc_info[312993] = ["eee", "12p5G", "lt10dd"]
mc_info[312994] = ["eee", "12p5G", "lt100dd"]
mc_info[312995] = ["eeu", "12p5G", "lt1dd"]
mc_info[312996] = ["eeu", "12p5G", "lt10dd"]
mc_info[312997] = ["eeu", "12p5G", "lt100dd"]
mc_info[312998] = ["eee", "15G", "lt1dd"]
mc_info[312999] = ["eee", "15G", "lt10dd"]
mc_info[313000] = ["eee", "15G", "lt100dd"]
mc_info[313001] = ["eeu", "15G", "lt1dd"]
mc_info[313002] = ["eeu", "15G", "lt10dd"]
mc_info[313003] = ["eeu", "15G", "lt100dd"]
mc_info[313004] = ["eee", "17p5G", "lt1dd"]
mc_info[313005] = ["eee", "17p5G", "lt10dd"]
mc_info[313006] = ["eee", "17p5G", "lt100dd"]
mc_info[313007] = ["eeu", "17p5G", "lt1dd"]
mc_info[313008] = ["eeu", "17p5G", "lt10dd"]
mc_info[313009] = ["eeu", "17p5G", "lt100dd"]
mc_info[313010] = ["eee", "20G", "lt1dd"]
mc_info[313011] = ["eee", "20G", "lt10dd"]
mc_info[313012] = ["eee", "20G", "lt100dd"]
mc_info[313013] = ["eeu", "20G", "lt1dd"]
mc_info[313014] = ["eeu", "20G", "lt10dd"]
mc_info[313015] = ["eeu", "20G", "lt100dd"]
mc_info[313419] = ["uee", "3G", "lt1dd"]
mc_info[313420] = ["uee", "3G", "lt10dd"]
mc_info[313421] = ["uee", "3G", "lt100dd"]
mc_info[313422] = ["euu", "3G", "lt1dd"]
mc_info[313423] = ["euu", "3G", "lt10dd"]
mc_info[313424] = ["euu", "3G", "lt100dd"]
mc_info[313425] = ["uee", "4G", "lt1dd"]
mc_info[313426] = ["uee", "4G", "lt10dd"]
mc_info[313427] = ["uee", "4G", "lt100dd"]
mc_info[313428] = ["euu", "4G", "lt1dd"]
mc_info[313429] = ["euu", "4G", "lt10dd"]
mc_info[313430] = ["euu", "4G", "lt100dd"]
mc_info[313431] = ["uee", "4p5G", "lt1dd"]
mc_info[313432] = ["uee", "4p5G", "lt10dd"]
mc_info[313433] = ["uee", "4p5G", "lt100dd"]
mc_info[313434] = ["euu", "4p5G", "lt1dd"]
mc_info[313435] = ["euu", "4p5G", "lt10dd"]
mc_info[313436] = ["euu", "4p5G", "lt100dd"]
mc_info[313437] = ["uee", "5G", "lt1dd"]
mc_info[313438] = ["uee", "5G", "lt10dd"]
mc_info[313439] = ["uee", "5G", "lt100dd"]
mc_info[313440] = ["euu", "5G", "lt1dd"]
mc_info[313441] = ["euu", "5G", "lt10dd"]
mc_info[313442] = ["euu", "5G", "lt100dd"]
mc_info[313443] = ["uee", "7p5G", "lt1dd"]
mc_info[313444] = ["uee", "7p5G", "lt10dd"]
mc_info[313445] = ["uee", "7p5G", "lt100dd"]
mc_info[313446] = ["euu", "7p5G", "lt1dd"]
mc_info[313447] = ["euu", "7p5G", "lt10dd"]
mc_info[313448] = ["euu", "7p5G", "lt100dd"]
mc_info[313449] = ["uee", "10G", "lt1dd"]
mc_info[313450] = ["uee", "10G", "lt10dd"]
mc_info[313451] = ["uee", "10G", "lt100dd"]
mc_info[313452] = ["euu", "10G", "lt1dd"]
mc_info[313453] = ["euu", "10G", "lt10dd"]
mc_info[313454] = ["euu", "10G", "lt100dd"]
mc_info[313455] = ["uee", "12p5G", "lt1dd"]
mc_info[313456] = ["uee", "12p5G", "lt10dd"]
mc_info[313457] = ["uee", "12p5G", "lt100dd"]
mc_info[313458] = ["euu", "12p5G", "lt1dd"]
mc_info[313459] = ["euu", "12p5G", "lt10dd"]
mc_info[313460] = ["euu", "12p5G", "lt100dd"]
mc_info[313461] = ["uee", "15G", "lt1dd"]
mc_info[313462] = ["uee", "15G", "lt10dd"]
mc_info[313463] = ["uee", "15G", "lt100dd"]
mc_info[313464] = ["euu", "15G", "lt1dd"]
mc_info[313465] = ["euu", "15G", "lt10dd"]
mc_info[313466] = ["euu", "15G", "lt100dd"]
mc_info[313467] = ["uee", "17p5G", "lt1dd"]
mc_info[313468] = ["uee", "17p5G", "lt10dd"]
mc_info[313469] = ["uee", "17p5G", "lt100dd"]
mc_info[313470] = ["euu", "17p5G", "lt1dd"]
mc_info[313471] = ["euu", "17p5G", "lt10dd"]
mc_info[313472] = ["euu", "17p5G", "lt100dd"]
mc_info[313473] = ["uee", "20G", "lt1dd"]
mc_info[313474] = ["uee", "20G", "lt10dd"]
mc_info[313475] = ["uee", "20G", "lt100dd"]
mc_info[313476] = ["euu", "20G", "lt1dd"]
mc_info[313477] = ["euu", "20G", "lt10dd"]
mc_info[313478] = ["euu", "20G", "lt100dd"]
mc_info[313479] = ["uue", "2p5G", "lt1dd"]
mc_info[313480] = ["uue", "2p5G", "lt10dd"]
mc_info[313481] = ["uue", "2p5G", "lt100dd"]
mc_info[313482] = ["eeu", "2p5G", "lt1dd"]
mc_info[313483] = ["eeu", "2p5G", "lt10dd"]
mc_info[313484] = ["eeu", "2p5G", "lt100dd"]
mc_info[313485] = ["utt", "10G", "lt1dd"]
mc_info[313486] = ["utt", "10G", "lt10dd"]
mc_info[313487] = ["utt", "10G", "lt100dd"]
mc_info[313488] = ["ett", "10G", "lt1dd"]
mc_info[313489] = ["ett", "10G", "lt10dd"]
mc_info[313490] = ["ett", "10G", "lt100dd"]

rtag_info = {
    'r11915' : 'mc16a',
    'r11916' : 'mc16d',
    'r11891' : 'mc16e',
    }

config_info = {
    'uuu' : '/home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/data/mc/config_mc_uuu.json',
    'uue' : '/home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/data/mc/config_mc_uue.json',
    'eeu' : '/home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/data/mc/config_mc_eeu.json',
    'eee' : '/home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/data/mc/config_mc_eee.json',
    'uee' : '/home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/data/mc/config_mc_uee.json', # mixed
    'euu' : '/home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/data/mc/config_mc_euu.json', # mixed
    
    # 'ett' : '/home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/data/mc/config_mc_eee.json', # tau
    # 'ett' : '/home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/data/mc/config_mc_eeu.json', # tau
    'ett' : '/home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/data/mc/config_mc_euu.json', # tau
    
    # 'utt' : '/home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/data/mc/config_mc_uuu.json', # tau
    # 'utt' : '/home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/data/mc/config_mc_uue.json', # tau
    'utt' : '/home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/data/mc/config_mc_uee.json', # tau
}

#____________________________________________________________
print("""#PBS -N dhnl_ntuple_analysis
#PBS -l walltime=48:00:00
#PBS -l nodes=1:ppn=1
#PBS -e /home/newhouse/tmp/grid_logs/dhnl_ntuple_analysis.err
#PBS -o /home/newhouse/tmp/grid_logs/dhnl_ntuple_analysis.log

source ~/public/Analysis/HNL/DHNLNtupleAnalysis/setup.sh

echo INPUT_FILE $INPUT_FILE
echo CONFIG_FILE $CONFIG_FILE
echo OUTPUT_FILE $OUTPUT_FILE

makeHistograms.py \\
--input $INPUT_FILE \\
--config $CONFIG_FILE \\
--output_file $OUTPUT_FILE \\
--saveNtuples OS \\
--force
""", 
file=open('run_grid.pbs', 'w'))
# --doSystematics \\

#____________________________________________________________
BATCH = True
OVERWRITE = False

indir = '/data/newhouse/HNL/ntuples/v9p0/'
outdir = '/data/hnl/histograms/v9p3_histograms_OS/'
os.makedirs(outdir, exist_ok=True)
# make input files using: for i in $(ls -1d /data/hnl/ntuples/v5p0_ntuples/mc/*); do ls $i/* -1d; done > input_files.txt
counter = 0
for infile in glob.glob(f'{indir}*/submitDir/data-tree/files.root'):
    # infile = infile.strip()
    # get mc generator info
    # read dsids from offical samples by splitting according to '.' in sample name
    if len([int(s) for s in infile.split(".") if s.isdigit()]) != 0:
        dsid = [int(s) for s in infile.split(".") if s.isdigit()][0]
    else:
        print("No DSID found for sample:", infile)
    
    # skip taus for now
    if mc_info[dsid][0] in ['ett', 'utt']:
        continue 
        
    # get reconstruction info
    rtag = infile.split('a875')[1].split('_')[1]
    # make output string
    outstring = f'{rtag_info[rtag]}_{mc_info[dsid][0]}_{mc_info[dsid][1]}_{mc_info[dsid][2].replace("lt","").replace("dd","mm")}'
    # name the output file
    outfile = outdir+'histograms_'+outstring+'.root'

    # skip if already done
    if not OVERWRITE:
        if os.path.isfile(outfile): continue

    # get the correct config file
    config = config_info[mc_info[dsid][0]]

    if BATCH:
        # set command with the correct input variables
        command  = f'qsub -v INPUT_FILE={infile},CONFIG_FILE={config},OUTPUT_FILE={outfile} -N ntuple_analysis_{outstring} -e /home/newhouse/tmp/grid_logs/ntuple_analysis_{outstring}.err -o /home/newhouse/tmp/grid_logs/ntuple_analysis_{outstring}.log /home/newhouse/public/Analysis/HNL/DHNLNtupleAnalysis/run/5_batch_submit_ntuples/run_grid.pbs'
        # run
        # print(command)
        exit_code = os.system(command)
        print(exit_code, 'running', outstring)

        time.sleep(0.1)

        # max_job_count=300
        # def run_count(): return int(os.popen('qstat -r|  wc -l').read().strip())
        # def idle_count(): return int(os.popen('qstat -i|  wc -l').read().strip())
        # while run_count() + idle_count() > max_job_count:
        #     time.sleep(10)
    else:
        print("makeHistograms.py",
            "--input", infile,
            "--config", config,
            "--output_file", outfile,
            "--saveNtuples", "DVtype",
            "--force")
        subprocess.Popen([
            "makeHistograms.py",
            "--input", infile,
            "--config", config,
            "--output_file", outfile,
            "--saveNtuples", "DVtype",
            "--force"])
        counter += 1
        if counter > 30: break