import glob
import os
from pathlib import Path


merged_files = list(set(
[x
.replace('mc16a_','') 
.replace('mc16d_','')
.replace('mc16e_','')  
for x in glob.glob('/data/hnl/histograms/v7p1_histograms/*')]
))
for x in merged_files:
    if 'histograms_all' in x: continue
    if 'fullrun2' in x: continue
    tokens = x.split('/')[-1].split('.root')[0].split('_')
    outfile = f'{Path(x).parent.absolute()}/histograms_fullrun2_{tokens[2]}_{tokens[3]}_{tokens[1]}.root'
    command = f'hadd -f {outfile} {x.replace("/histograms_", "/histograms_mc16*_")}'
    print('____________________________________________________________')
    print(command)
    exit_code = os.system(command)
    print(exit_code, ':', command)
    # break